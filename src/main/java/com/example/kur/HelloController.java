package com.example.kur;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import static javafx.application.Application.launch;

public class HelloController implements Initializable {
    @FXML
    private TableView table;
    @FXML
    private TextField otkuda;
    @FXML
    private TextField kuda;
    @FXML
    private DatePicker skakogo;
    @FXML
    private DatePicker obratno;
    @FXML
    private Label error;
    @FXML
    private ComboBox enter;
    @FXML
    private Button poislk;
    //подключаемся к классам
    Transition transition = new Transition();

    DB db = new DB();

    ticket ticket = new ticket();


    // то, что будет показываться сразу при загрузки страницы
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {


        enter.getItems().addAll("Авторизация", "Регистрация");



    }
    // переход на регистрацию и авторизацию

    public void clickEnter(ActionEvent event) throws SQLException, ClassNotFoundException, IOException {
        //если пользователь выбирает кнопку "Авторизация" или "Регистрация"
        if(enter.getValue().equals("Авторизация")){
            transition.onClose("avtorization", poislk);
        }else {
            transition.onClose("registration", poislk);
        }
    }



    //поиск билетов по введенным данным пользователя
    public void onSearch(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        //очищаем таблицу
        table.getItems().clear();
        //делаем таблицу видимой
        table.setVisible(true);
        //проверяем вводимые поля на заполненность
        if (otkuda.getText().equals("") || kuda.getText().equals("") || skakogo.getValue().equals("") || obratno.getValue().equals("")){
                error.setText("Введите, пожалуйста, все данные");
        }else{
            //если все поля заполнены будет показана таблица с нужными билетами
            ArrayList<String> strings = db.getTicket(otkuda.getText(), kuda.getText(), skakogo.getValue(), obratno.getValue());
            ticket.createTicket(table, strings);
        }



    }
}