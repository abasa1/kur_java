package com.example.kur;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

public class Registration {
    @FXML
    public Button enter;
    @FXML
    public TextField login;
    @FXML
    public TextField password;
    @FXML
    public PasswordField passwordF;
    @FXML
    public Label error;
    @FXML
    public CheckBox openPassword;

    //подключение к классу
    DB db = new DB();
    Transition transition = new Transition();
    //проверка пользователя в системе, занесение в базу данных и переход в личный кабинет
    @FXML
    private void onEnter(ActionEvent actionEvent) throws IOException, SQLException, ClassNotFoundException {

        String passwordText = password.getText();
        String passwordFieldText = passwordF.getText();
        String loginText = login.getText();
        ArrayList<String> loginArray = db.getLoginUser();
        int user = 0;
        if(passwordFieldText.equals("") || loginText.equals("")){
                error.setText("Введены не все данные");

        } else {
            for(int i = 0; loginArray.size() > i; i++) {
                //если пользователь с таким логином есть
                if (loginText.equals(loginArray.get(i))) {
                    user++;
                    error.setText("Пользователь с таким логином зарегистрирован");
                }

            }
            if(user==0){
                Integer id = db.getMaxIdUser()+1;
                if(passwordFieldText.equals("")){
                    db.getInsertUser(id, loginText, passwordText);
                    transition.onClose("user", enter);
                }else{
                    db.getInsertUser(id, loginText, passwordFieldText);
                    transition.onClose("user", enter);

                }
            }

        }

    }

    //показать или скрыть пароль

    public void onOpenPassword(){
        if(openPassword.isSelected()){
            String passwordText = passwordF.getText();
            passwordF.setVisible(false);
            password.setText(passwordText);
            passwordF.clear();
        }else{
            String passwordText = password.getText();
            passwordF.setVisible(true);
            passwordF.setText(passwordText);
            password.clear();
        }
    }
}
