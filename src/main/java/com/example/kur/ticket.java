package com.example.kur;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.util.Callback;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class ticket  {

    public final TableView<Data> table = new TableView<>();
    public final ObservableList<Data> tvObservableList = FXCollections.observableArrayList();
    private Window primaryStage;
    private static final int CANVAS_SIZE = 150;
    DB db = new DB();
    public static String idTicket;

    //создание таблицы
    public void createTicket(TableView<Data> table, ArrayList<String> list) {

            setTableappearance();

            fillTableObservableListWithSampleData(list);
            table.setItems(tvObservableList);

        //даем названия столбцам
            TableColumn<Data, String> timeP = new TableColumn<>("Время посадки");
            timeP.setCellValueFactory(new PropertyValueFactory<>("timeP"));

            TableColumn<Data, String> dataP = new TableColumn<>("Дата посадки");
            dataP.setCellValueFactory(new PropertyValueFactory<>("dataP"));

            TableColumn<Data, String> timePr = new TableColumn<>("Время прибытия");
            timePr.setCellValueFactory(new PropertyValueFactory<>("timePr"));

            TableColumn<Data, String> dataPr = new TableColumn<>("Дата прибытия");
            dataPr.setCellValueFactory(new PropertyValueFactory<>("dataPr"));

            TableColumn<Data, String> cost = new TableColumn<>("Стоимость");
            cost.setCellValueFactory(new PropertyValueFactory<>("cost"));

            TableColumn<Data, String> id = new TableColumn<>("ID");
            id.setCellValueFactory(new PropertyValueFactory<>("id"));
            id.setVisible(false);

            //добавляем в таблицу столбцы
            table.getColumns().addAll(timeP, dataP, timePr, dataPr, cost, id);


        }

        //настраиваем ей размеры
        private void setTableappearance() {
            table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            table.setPrefWidth(600);
            table.setPrefHeight(500);
        }

        //внесение данных в таблицу
        private void fillTableObservableListWithSampleData(ArrayList<String> list) {

        for(int i = 0; list.size() > i; i++){

            tvObservableList.addAll(new Data(list.get(i), list.get(++i), list.get(++i), list.get(++i), list.get(++i), list.get(++i)));
        }


        }
        // формирование кнопки для продавца
       public void addButtonToTable(TableView<Data> table, Canvas canvas) {
            TableColumn<Data, Void> colBtn = new TableColumn(" ");

            Callback<TableColumn<Data, Void>, TableCell<Data, Void>> cellFactory = new Callback<TableColumn<Data, Void>, TableCell<Data, Void>>() {
                @Override
                public TableCell<Data, Void> call(final TableColumn<Data, Void> param) {
                    final TableCell<Data, Void> cell = new TableCell<Data, Void>() {

                        private final Button btn = new Button("Купить");

                        {
                            btn.setOnAction((ActionEvent event) -> {

                                Data data = getTableView().getItems().get(getIndex());
                                barcode(data.id, data.timeP, canvas);
                                idTicket= data.id;
                                try {
                                    db.getUpdateCountTicket(idTicket);
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                } catch (ClassNotFoundException e) {
                                    e.printStackTrace();
                                }
                            });
                        }

                        @Override
                        public void updateItem(Void item, boolean empty) {
                            super.updateItem(item, empty);
                            if (empty) {
                                setGraphic(null);
                            } else {
                                setGraphic(btn);
                            }
                        }
                    };
                    return cell;
                }
            };

            colBtn.setCellFactory(cellFactory);

            table.getColumns().add(colBtn);

       }
       //формирование кнопки для зарегистрированного пользователя
    public void addUser(TableView<Data> table) {
        TableColumn<Data, Void> colBtn = new TableColumn(" ");

        Callback<TableColumn<Data, Void>, TableCell<Data, Void>> cellFactory = new Callback<TableColumn<Data, Void>, TableCell<Data, Void>>() {
            @Override
            public TableCell<Data, Void> call(final TableColumn<Data, Void> param) {
                final TableCell<Data, Void> cell = new TableCell<Data, Void>() {

                    private final Button btn = new Button("Купить");

                    {
                        btn.setOnAction((ActionEvent event) -> {

                            Data data = getTableView().getItems().get(getIndex());
                            idTicket= data.id;
                            try {
                                pdfUser(Integer.valueOf(idTicket));
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (DocumentException e) {
                                e.printStackTrace();
                            } catch (SQLException e) {
                                e.printStackTrace();
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                            try {
                                db.getUpdateCountTicket(idTicket);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(btn);
                        }
                    }
                };
                return cell;
            }
        };

        colBtn.setCellFactory(cellFactory);

        table.getColumns().add(colBtn);

    }

    //инициализация данных вносимых в таблицу
        public class Data {

            private String timeP;
            private String dataP;
            private String timePr;
            private String dataPr;
            private String id;


            private String cost;
            private Data(String timeP, String dataP, String timePr, String dataPr, String cost, String id) {
                this.timeP=timeP;
                this.dataP=dataP;
                this.timePr=timePr;
                this.dataPr=dataPr;
                this.cost=cost;
                this.id=id;
            }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDataP() {
                return dataP;
            }

            public String getDataPr() {
                return dataPr;
            }

            public String getCost() {
                return cost;
            }

            public String getTimeP() {
                return timeP;
            }

            public void setDataP(String dataP) {
                this.dataP = dataP;
            }

            public String getTimePr() {
                return timePr;
            }

            public void setDataPr(String dataPr) {
                this.dataPr = dataPr;
            }

            public void setCost(String cost) {
                this.cost = cost;
            }

            public void setTimeP(String timeP) {
                this.timeP = timeP;
            }

            public void setTimePr(String timePr) {
                this.timePr = timePr;
            }
        }


    public void barcode(String id, String time, Canvas canvas){
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        double mm = (double) Toolkit.getDefaultToolkit().getScreenResolution() / 25.4D;
        System.out.println(mm);
        int[] palks = new int[]{5, 1, 4, 0, 9, 2, 0, 2, 0, 1, 2, 3, 4, 5, 6};
        gc.setFill(javafx.scene.paint.Color.BLACK);
        int x0 = 20;
        int y0 = 10;
        double heightPalks = 22.85D * mm;
        double weightOgrPalks = 0.5D * mm;
        double rasstoyanieMegdyPalk = 0.5D * mm;

        gc.fillRect((double)x0, (double)y0, weightOgrPalks, heightPalks + 1.65D * mm);
        int otcydaPalks = (int)((double)x0 + weightOgrPalks + rasstoyanieMegdyPalk);
        int otcydaZifra = otcydaPalks;
        gc.fillRect((double)otcydaPalks, (double)y0, weightOgrPalks, heightPalks + 1.65D * mm);
        otcydaPalks = (int)((double)otcydaPalks + weightOgrPalks + rasstoyanieMegdyPalk);
        boolean printSrednyaPalks = false;

        for(int numberPalks = 0; numberPalks < palks.length; ++numberPalks) {
            double shirinaPalks = (double)palks[numberPalks] * 0.15D * mm;
            if (numberPalks == palks.length / 2 && !printSrednyaPalks) {
                gc.setFill(javafx.scene.paint.Color.BLACK);
                gc.fillRect((double)otcydaPalks, (double)y0, weightOgrPalks, heightPalks + 1.65D * mm);
                otcydaPalks = (int)((double)otcydaPalks + weightOgrPalks + rasstoyanieMegdyPalk);

                gc.fillRect((double)otcydaPalks, (double)y0, weightOgrPalks, heightPalks + 1.65D * mm);
                otcydaPalks = (int)((double)otcydaPalks + weightOgrPalks + rasstoyanieMegdyPalk);
                --numberPalks;
                printSrednyaPalks = true;

            } else {
                if (shirinaPalks == 0.0D) {
                    shirinaPalks = 1.35D * mm;
                    gc.setFill(javafx.scene.paint.Color.WHITE);
                } else {
                    gc.setFill(Color.BLACK);
                }

                gc.fillRect((double)otcydaPalks, (double)y0, shirinaPalks, heightPalks);

                otcydaPalks = (int)((double)otcydaPalks + shirinaPalks + rasstoyanieMegdyPalk);
            }
        }
        //цифры на штри коде состоят из id билета, даты формирования билета, время отлета
        gc.fillRect((double)otcydaPalks, (double)y0, weightOgrPalks, heightPalks + 1.65D * mm);
        otcydaPalks = (int)((double)otcydaPalks + weightOgrPalks + rasstoyanieMegdyPalk);
        gc.fillRect((double)otcydaPalks, (double)y0, weightOgrPalks, heightPalks + 1.65D * mm);
        String timeStamp = new SimpleDateFormat("ddMMyyyyHHmm").format(Calendar.getInstance().getTime());
        String codes = id+timeStamp+time;
        for(int i = 0; i < 6; i++){
            int a = (int) (Math.random()*10);
            codes+= a;
        }
        gc.fillText(codes, x0, y0+heightPalks + 1.65D * mm + 10, 80);

    }

    //создание pdf файла для продавца
    public void pdf(Canvas canvas, Integer idDB) throws URISyntaxException, IOException, DocumentException, SQLException, ClassNotFoundException {

        //подключаем шрифт
        Font font = FontFactory.getFont("src/DejaVuSans.ttf", "cp1251", BaseFont.EMBEDDED, 10);
        //забираем картинку со штрих кодом
        File file = new File("src/main/resources/shtrih-code.jpeg");

        //если штрих-код сформирован
        if (file != null) {
            try {
                WritableImage writableImage = new WritableImage(CANVAS_SIZE, CANVAS_SIZE);
                canvas.snapshot(null, writableImage);

                RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                ImageIO.write(renderedImage, "png", file);
            } catch (IOException ex) {
                ex.printStackTrace();
                System.out.println("Error!");
            }
        }
        Path path1 = Paths.get(ClassLoader.getSystemResource("shtrih-code.jpeg").toURI());
        //создаем документ
        Document document1 = new Document();
        //даем ему название и место сохранения
        PdfWriter.getInstance(document1, new FileOutputStream("file.pdf"));
        //открываем документ
        document1.open();
        //заносим в список данные для билета из базы данных
        ArrayList<String> lits = db.getBilet(idDB);
        //переводим файл в картинку
        com.itextpdf.text.Image image = com.itextpdf.text.Image.getInstance(path1.toAbsolutePath().toString());
        //заносим текст, который будет отображаться на билете в Paragraph
        Paragraph paragraphOne = new Paragraph("Рейс: " + lits.get(0)+" - "+lits.get(1), font);
        Paragraph paragraphOne1 = new Paragraph("Время: " + lits.get(2), font);
        Paragraph paragraphOne2= new Paragraph("Стоимость: " + lits.get(3), font);
        //добавляем Paragraph в документ
       document1.add(paragraphOne);
       document1.add(paragraphOne1);
       document1.add(paragraphOne2);
       //добавляем картинку в документ
        document1.add(image);
        //закрываем документ
        document1.close();
        //открываем файл после загрузки
        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File( "file.pdf");
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                // no application registered for PDFs
            }
        }
    }

    //создание pdf файла для пользователя
    public void pdfUser(Integer idDB) throws URISyntaxException, IOException, DocumentException, SQLException, ClassNotFoundException {
        FileChooser savefile = new FileChooser();
        savefile.setTitle("Save File");
        Font font = FontFactory.getFont("src/DejaVuSans.ttf", "cp1251", BaseFont.EMBEDDED, 10);

        File file1 = savefile.showSaveDialog(primaryStage);
        Document document1 = new Document();
        PdfWriter.getInstance(document1, new FileOutputStream(String.valueOf(file1)));
        document1.open();
        ArrayList<String> lits = db.getBilet(idDB);
        Paragraph paragraphOne = new Paragraph("Рейс: " + lits.get(0)+" - "+lits.get(1), font);
        Paragraph paragraphOne1 = new Paragraph("Время: " + lits.get(2), font);
        Paragraph paragraphOne2= new Paragraph("Стоимость: " + lits.get(3), font);
        document1.add(paragraphOne);
        document1.add(paragraphOne1);
        document1.add(paragraphOne2);

        document1.close();
    }

}
