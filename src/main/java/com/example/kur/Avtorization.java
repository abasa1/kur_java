package com.example.kur;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.IOException;
import java.sql.SQLException;

public class Avtorization {

    @FXML
    public Button enter;
    @FXML
    public TextField login;
    @FXML
    public TextField password;
    @FXML
    public PasswordField passwordF;
    @FXML
    public Label error;
    @FXML
    public CheckBox openPassword;
    // подключаем классы
    DB db = new DB();
    Transition transition = new Transition();
    //проверка пользователя в системе и перевод его в окно, зависимо от его роли от его роли
    @FXML
    private void onEnter(ActionEvent actionEvent) throws IOException, SQLException, ClassNotFoundException {
        // записываем значения, которые ввел пользователь
        String passwordText = password.getText();
        String passwordFieldText = passwordF.getText();
        String loginText = login.getText();
        //отправляем запрос к базе данных на нахождение такого пользователя в базе
        Integer[] registration = db.getRegistration( loginText, passwordFieldText);
        Integer[] registration1 = db.getRegistration( loginText, passwordText);

        //есди такой пользователь в базе данных есть
        if(registration[0] == 1 || registration1[0]==1){
            //устанавливаем его роль и перенаправляем на соответствующее окно
            if(registration[1]==1 || registration1[1]==1){
                transition.onClose("administrator", enter);
            }else if(registration[1]==2 || registration1[1]==2){
                transition.onClose("seller", enter);
            }else if(registration[1]==0 || registration1[1]==0){
                transition.onClose("user", enter);
            }

        }else {
            error.setText("Такого пользователя нет");
        }


    }

    //показать или скрыть пароль
    public void onOpenPassword(){
        if(openPassword.isSelected()){
            String passwordText = passwordF.getText();
            passwordF.setVisible(false);
            password.setText(passwordText);
        }else{
            String passwordText = password.getText();
            passwordF.setVisible(true);
            passwordF.setText(passwordText);
        }
    }
}
