package com.example.kur;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

public class DB {
    private Connection connection = null;


    // подключение к базе данных
    private Connection getDbConnection() throws ClassNotFoundException, SQLException {
        Map<String, String> env = System.getenv();
        String host = env.getOrDefault("DB_HOST", "localhost");
        ;
        String port = env.getOrDefault("DB_PORT", "3306");
        String database = env.getOrDefault("DB_NAME", "mysql");
        String user = env.getOrDefault("DB_USER", "root");
        String password = env.getOrDefault("DB_PASS", "root");

        String connectionUrl = String.format(
                "jdbc:mysql://%s:%s/%s?serverTimezone=UTC",
                host,
                port,
                database
        );

        System.out.println(connectionUrl);

        if (connection == null) {
            connection = DriverManager.getConnection(
                    connectionUrl,
                    user,
                    password
            );
            System.out.println("Connected to database");
        }

        String s = new String();
        StringBuffer sb = new StringBuffer();

        try {
            FileReader fr = new FileReader(new File("backup.sql"));
            // be sure to not have line starting with "--" or "/*" or any other non aplhabetical character

            BufferedReader br = new BufferedReader(fr);

            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
            br.close();

            // here is our splitter ! We use ";" as a delimiter for each request
            // then we are sure to have well formed statements
            String[] inst = sb.toString().split(";");

            Connection c = connection;
            Statement st = c.createStatement();

            for (int i = 0; i < inst.length; i++) {
                // we ensure that there is no spaces before or after the request string
                // in order to not execute empty statements
                if (!inst[i].trim().equals("")) {
                    st.executeUpdate(inst[i]);
                    System.out.println(">>" + inst[i]);
                }
            }

        } catch (Exception e) {
            System.out.println("*** Error : " + e.toString());
            System.out.println("*** ");
            System.out.println("*** Error : ");
            e.printStackTrace();
            System.out.println("################################################");
            System.out.println(sb.toString());
        }


        return connection;
    }

    // выводим информацию о билетах на первой страницу
    public ArrayList<String> getTicket(String dataI, String dataO, LocalDate timeI, LocalDate timeO) throws SQLException, ClassNotFoundException {
        String sql1 = "SELECT timeFrom, departureDate, timeWhere, arrivalDate, cost, id FROM korolyov_da_application_db.tickets as tic where " +
                "(whereFrom='" + dataI + "' or whereFrom='" + dataO + "') and (tic.where='" + dataO + "' or tic.where='" + dataI + "')" +
                "and (departureDate='" + timeI + "' or departureDate='" + timeO + "' ) and count>0";
        Statement statement = getDbConnection().createStatement();
        System.out.println(sql1);
        ResultSet resultSet = statement.executeQuery(sql1);
        ArrayList<String> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(resultSet.getString(1));
            result.add(resultSet.getString(2));
            result.add(resultSet.getString(3));
            result.add(resultSet.getString(4));
            result.add(resultSet.getString(5));
            result.add(resultSet.getString(6));


        }
        statement.close();
        return result;

    }

    //проверяем пользователя зарегистрированн ли он в системе
    public Integer[] getRegistration(String login, String password) throws SQLException, ClassNotFoundException {
        String sql1 = "SELECT count(*), post FROM korolyov_da_application_db.staff where " +
                "staff.login='" + login + "' and staff.password='" + password + "'";
        String sql2 = "SELECT count(*), 0 FROM korolyov_da_application_db.user where user.login='" + login + "' and user.password='" + password + "'";
        Statement statement = getDbConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(sql1);
        Integer[] result = new Integer[2];
        resultSet.next();
        if (resultSet.getString(1).equals("1")) {
            resultSet = statement.executeQuery(sql1);
        } else {
            resultSet = statement.executeQuery(sql2);
        }
        System.out.println(sql1);
        System.out.println(sql2);

        while (resultSet.next()) {
            result[0] = Integer.valueOf(resultSet.getString(1));
            result[1] = Integer.valueOf(resultSet.getString(2));

        }


        System.out.println(result[1]);
        return result;

    }

    //выводим информацию о билетах для пользователей вошедших в личный кабинет
    public ArrayList<String> getTicketAdministrator() throws SQLException, ClassNotFoundException {
        String sql1 = "SELECT timeFrom, departureDate, timeWhere, arrivalDate, cost, id FROM korolyov_da_application_db.tickets where count>0";
        Statement statement = getDbConnection().createStatement();
        System.out.println(sql1);
        ResultSet resultSet = statement.executeQuery(sql1);
        ArrayList<String> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(resultSet.getString(1));
            result.add(resultSet.getString(2));
            result.add(resultSet.getString(3));
            result.add(resultSet.getString(4));
            result.add(resultSet.getString(5));
            result.add(resultSet.getString(6));


        }
        return result;
    }

    //выводим все должности в компании
    public ArrayList<String> getPost() throws SQLException, ClassNotFoundException {
        String sql1 = "SELECT name FROM korolyov_da_application_db.post";
        Statement statement = getDbConnection().createStatement();
        System.out.println(sql1);
        ResultSet resultSet = statement.executeQuery(sql1);
        ArrayList<String> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(resultSet.getString(1));

        }
        return result;
    }

    //запрос на добавление новго сотрудника
    public void getInsert(Integer id, String fio, String address, String pasport, String telephone, String post, String login, String password) throws SQLException, ClassNotFoundException {
        int idPost = 0;
        if (post.equals("Администратор")) {
            idPost = 1;
        } else if (post.equals("Продавец")) {
            idPost = 2;
        }
        String sql1 = " INSERT INTO `korolyov_da_application_db`.`staff` (`id`, `FIO`, `address`, `passport`, `telephone`, `post`, `login`, `password`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement pst;
        pst = connection.prepareStatement(sql1);
        pst.setInt(1, id);
        pst.setString(2, fio);
        pst.setString(3, address);
        pst.setString(4, pasport);
        pst.setString(5, telephone);
        pst.setInt(6, idPost);
        pst.setString(7, login);
        pst.setString(8, password);
        pst.executeUpdate();

    }


    //ищем максимальное id сотрудника
    public Integer getMaxId() throws SQLException, ClassNotFoundException {
        String zapros = "SELECT max(id) FROM korolyov_da_application_db.staff";
        Statement statement = getDbConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(zapros);
        int result = 0;

        while (resultSet.next()) {
            result = resultSet.getInt(1);
        }
        statement.close();
        return result;
    }

    //выводим все логины пользователей в базе данных
    public ArrayList<String> getLoginUser() throws SQLException, ClassNotFoundException {
        String sql1 = "SELECT login FROM korolyov_da_application_db.user";
        Statement statement = getDbConnection().createStatement();
        System.out.println(sql1);
        ResultSet resultSet = statement.executeQuery(sql1);
        ArrayList<String> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(resultSet.getString(1));

        }
        return result;
    }

    //запрос на добавление новго пользователя
    public void getInsertUser(Integer id, String login, String password) throws SQLException, ClassNotFoundException {

        String sql1 = "INSERT INTO `korolyov_da_application_db`.`user` (`id`, `login`, `password`) VALUES (?, ?, ?)";
        PreparedStatement pst;
        pst = connection.prepareStatement(sql1);
        pst.setInt(1, id);
        pst.setString(2, login);
        pst.setString(3, password);
        pst.executeUpdate();

    }

    //ищем максимальное id пользователя
    public Integer getMaxIdUser() throws SQLException, ClassNotFoundException {
        String zapros = "SELECT max(id) FROM korolyov_da_application_db.user";
        Statement statement = getDbConnection().createStatement();
        ResultSet resultSet = statement.executeQuery(zapros);
        int result = 0;

        while (resultSet.next()) {
            result = resultSet.getInt(1);
        }
        statement.close();
        return result;
    }

    //выводм данные для формирование билета
    public ArrayList<String> getBilet(Integer id) throws SQLException, ClassNotFoundException {
        String sql1 = "select whereFrom, tickets.where, timeFrom, cost from korolyov_da_application_db.tickets where id = '" + id + "'";
        Statement statement = getDbConnection().createStatement();
        System.out.println(sql1);
        ResultSet resultSet = statement.executeQuery(sql1);
        ArrayList<String> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(resultSet.getString(1));
            result.add(resultSet.getString(2));
            result.add(resultSet.getString(3));
            result.add(resultSet.getString(4));

        }
        return result;
    }

    //обновляем количество билетов в базе при покупке
    public void getUpdateCountTicket(String id) throws SQLException, ClassNotFoundException {

        String sql1 = " UPDATE `korolyov_da_application_db`.`tickets` SET `count` = `count`-1 WHERE (`id` = '" + id + "');";
        Statement statement = getDbConnection().createStatement();
        statement.executeUpdate(sql1);


    }


}
