package com.example.kur;

import com.itextpdf.text.DocumentException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class User implements Initializable {
    @FXML
    private TableView table;
    @FXML
    private Button exit;



    ticket ticket = new ticket();
    DB db = new DB();
    Transition transition = new Transition();

    //выход из профиля на главную страницу
    public void onExit(ActionEvent actionEvent) throws IOException {
        transition.onClose("hello-view", exit);
    }

    //загрузка таблицы с кнопкой
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            ticket.createTicket(table, db.getTicketAdministrator());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        ticket.addUser(table);


    }
}
