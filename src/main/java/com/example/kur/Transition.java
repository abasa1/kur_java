package com.example.kur;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;

import javafx.stage.Stage;

import java.io.IOException;

public class Transition {
    //переход на новое окно с закрытием предыдущего
    public void onClose(String fxml, Button button) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource(fxml+".fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 866, 551);
        stage.getIcons().add(new Image("file:src/img/home.png"));
        stage.setScene(scene);
        // задаем минимальные и максимальные размеры окна
        stage.setMaxHeight(800);
        stage.setMaxWidth(800);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.show();
        //закрываем предыдущее
        Stage stageNow = (Stage) button.getScene().getWindow();
        stageNow.close();



    }

    //переход на новую страницу без закрывания предыдущего окна
    public void onTransition(String fxml) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource(fxml+".fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 866, 551);
        stage.getIcons().add(new Image("file:src/img/home.png"));
        stage.setScene(scene);
        stage.setMaxHeight(800);
        stage.setMaxWidth(800);
        stage.setMinHeight(500);
        stage.setMinWidth(700);
        stage.show();


    }
}
