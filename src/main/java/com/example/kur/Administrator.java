package com.example.kur;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class Administrator implements Initializable {

    @FXML
    private TableView table;
    @FXML
    private Button exit;
    @FXML
    private Canvas canvas;


    //подключаемся к классам
    ticket ticket = new ticket();
    DB db = new DB();
    Transition transition = new Transition();

    //выход из профиля на главную страницу
    public void onExit(ActionEvent actionEvent) throws IOException {
        transition.onClose("hello-view", exit);
    }

    //загрузка
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // выводтим таблицу со всеми билетами
        try {
            ticket.createTicket(table, db.getTicketAdministrator());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    //переход на новое окно добавления сотрудника при нажатии на кнопку
    public void onNewPersonal(ActionEvent actionEvent) throws IOException {
        transition.onTransition("newPersonal");
    }
}
