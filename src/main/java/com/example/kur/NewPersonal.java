package com.example.kur;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class NewPersonal implements Initializable {
    @FXML
    private TextField fio;
    @FXML
    private TextArea address;
    @FXML
    private TextField pasport;
    @FXML
    private TextField telefone;
    @FXML
    private TextField login;
    @FXML
    private TextField password;
    @FXML
    private ComboBox post;
    @FXML
    private Label error;

    //подключение к классу
    DB db = new DB();
    // добавляем значения в ComboBox при загрузке окна
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            ArrayList<String> postFilter = db.getPost();
            post.getItems().addAll(postFilter);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void onInsertPersonal(ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        //берем введенные данные
        String fioText = fio.getText();
        String addressText = address.getText();
        String pasportText = pasport.getText();
        String telefoneTetx = telefone.getText();
        String loginText = login.getText();
        String passwordText = password.getText();
        //проверяем на заполненность
        if(fioText.equals("") || addressText.equals("")|| pasportText.equals("")|| telefoneTetx.equals("")|| loginText.equals("")|| passwordText.equals("")){
            error.setText("Введены не все данные");

        }else {
            //если все заполнено, вносим пользователя в базу
            int id = db.getMaxId()+1;
            db.getInsert(id, fioText, addressText,pasportText, telefoneTetx, (String) post.getValue(), loginText, passwordText);
            error.setText("Сотрудник добавлен");

        }
    }
}
