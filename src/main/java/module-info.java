module com.example.kur {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires java.desktop;
    requires itextpdf;
    requires javafx.swing;


    opens com.example.kur to javafx.fxml;
    exports com.example.kur;
}