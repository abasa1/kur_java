#!/bin/bash
xhost +;
docker network create net_korolyov_da;
docker rm -f korolyov_da_container_db korolyov_da_container_app;
docker run --name=korolyov_da_container_db --hostname=korolyov_da-container-db -e MARIADB_ROOT_PASSWORD=root -e MARIADB_DATABASE=korolyov_da_application_db -e MARIADB_USER=korolyov_da_application_user -e MARIADB_PASSWORD=korolyov_da_application_pass --network net_korolyov_da -d mariadb;
sleep 5s;
docker run -it -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --name=korolyov_da_container_app --hostname=korolyov_da-container-app -e DB_HOST=korolyov_da_container_db -e DB_PORT=3306 -e DB_NAME=korolyov_da_application_db -e DB_USER=korolyov_da_application_user -e DB_PASS=korolyov_da_application_pass --network net_korolyov_da korolyovda/coursework_java;
