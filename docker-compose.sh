docker rm -f korolyov_da_container_db korolyov_da_container_app;

rm -rf compose_korolyovda
mkdir compose_korolyovda
cd compose_korolyovda
cat << EOF > docker-compose.yaml
version: '3.9'

services:
  korolyov_da_container_db:
    container_name: "korolyov_da_container_db"
    hostname: "korolyov_da-container-db"
    image: "mariadb"
    restart: "always"
    environment:
      MARIADB_ROOT_PASSWORD: "root"
      MARIADB_DATABASE: "korolyov_da_application_db"
      MARIADB_USER: korolyov_da_application_user
      MARIADB_PASSWORD: korolyov_da_application_pass
    networks:
      - net_korolyov_da

  korolyov_da-container-app:
    container_name: "korolyov_da_container_app"
    hostname: "korolyov_da-container-app"
    image: "korolyovda/coursework_java"
    restart: "no"
    environment:
      DISPLAY: "$DISPLAY"
      DB_HOST: korolyov_da_container_db
      DB_PORT: "3306"
      DB_NAME: korolyov_da_application_db
      DB_USER: korolyov_da_application_user
      DB_PASS: korolyov_da_application_pass
    volumes:
      - /tmp/.X11-unix:/tmp/.X11-unix
    networks:
      - net_korolyov_da
    depends_on:
      - korolyov_da_container_db

  phpmyadmin_korolyovda:
    container_name: "phpmyadmin_korolyovda"
    hostname: "phpmyadmin_korolyovda"
    image: phpmyadmin
    restart: always
    ports:
      - 8080:80
    environment:
      - PMA_ARBITRARY=1
    networks:
      - net_korolyov_da

networks:
  net_korolyov_da:
    name: net_korolyov_da
EOF

docker-compose up -d