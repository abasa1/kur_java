CREATE SCHEMA IF NOT EXISTS `korolyov_da_application_db`;
USE `korolyov_da_application_db` ;


CREATE TABLE IF NOT EXISTS `korolyov_da_application_db`.`post` (
  `id` INT NOT NULL,
  `name` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));



CREATE TABLE IF NOT EXISTS `korolyov_da_application_db`.`staff` (
  `id` INT NOT NULL,
  `FIO` VARCHAR(45) NOT NULL,
  `address` VARCHAR(100) NOT NULL,
  `passport` INT NOT NULL,
  `telephone` INT NOT NULL,
  `post` INT NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_staff_post` (`post` ASC) VISIBLE,
  CONSTRAINT `fk_staff_post`
    FOREIGN KEY (`post`)
    REFERENCES `korolyov_da_application_db`.`post` (`id`));



CREATE TABLE IF NOT EXISTS `korolyov_da_application_db`.`tickets` (
  `id` INT NOT NULL,
  `departureDate` DATE NOT NULL,
  `arrivalDate` DATE NOT NULL,
  `whereFrom` VARCHAR(45) NOT NULL,
  `where` VARCHAR(45) NOT NULL,
  `cost` DECIMAL(10,2) NOT NULL,
  `travelTime` VARCHAR(45) NOT NULL,
  `timeFrom` TIME NOT NULL,
  `timeWhere` TIME NOT NULL,
  `count` INT NOT NULL,
  PRIMARY KEY (`id`));



CREATE TABLE IF NOT EXISTS `korolyov_da_application_db`.`user` (
  `id` INT NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
